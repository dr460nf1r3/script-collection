#!/bin/bash
docker-compose -f mumble-docker-compose.yml up -d
docker-compose -f nginx-docker-compose.yml up -d
docker-compose -f portainer-docker-compose.yml up -d
docker-compose -f traefik-docker-compose.yml up -d
docker-compose -f wikijs-docker-compose.yml up -d
docker-compose -f whoogle-docker-compose.yml up -d
docker-compose -f jellyfin-docker-compose.yml up -d
docker-compose -f heimdall-docker-compose.yml up -d
docker-compose -f bitwarden-docker-compose.yml up -d
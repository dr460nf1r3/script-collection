#!/bin/bash
# A little script to download spotify/youtube stuff
function goto
{
    label=$1
    cmd=$(sed -n "/^:[[:blank:]][[:blank:]]*${label}/{:a;n;p;ba};" $0 | 
          grep -v ':$')
    eval "$cmd"
    exit
}

function playlist
{
	spotdl --playlist $link --overwrite skip
}

function album
{
	spotdl --album $link --overwrite skip
}

function song
{
	spotdl --song $link --overwrite skip
}

echo "Please enter the link of the content you want to download:"

: start
read link

echo "What content type do you want to download?"
echo "1) Playlist"
echo "2) Album"
echo "3) Song"

read content

if [ $content = "1" ]; then
	playlist
elif [ $content = "2" ]; then
	album
elif [ $content = "3" ]; then
	song
else echo "Invalid selection, please restart"
	goto start
fi

spotdl --list *.txt
rm *.txt

echo "Content successfully downloaded!"


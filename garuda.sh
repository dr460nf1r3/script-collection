#!/bin/bash
gnome-extensions disable material-shell@papyelgringo
gnome-extensions disable dash-to-panel@jderose9.github.com
gnome-extensions disable pop-shell@system76.com
gnome-extensions enable dash-to-dock@micxgx.gmail.com
gnome-extensions enable arc-menu@linxgem33.com
gnome-extensions enable unite@hardpixel.eu
gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com
gnome-extensions enable transparent-window-moving@noobsai.github.com
gnome-extensions enable transparent-panel@fthx

# Always enable those
gnome-extensions enable appindicatorsupport@rgcjonas.gmail.com
gnome-extensions enable arch-update@RaphaelRochet
gnome-extensions enable blyr@yozoon.dev.gmail.com
gnome-extensions enable gamemode@christian.kellner.me
gnome-extensions enable goodbyegdmflick@pratap.fastmail.fm
gnome-extensions enable gsconnect@andyholmes.github.io
gnome-extensions enable noannoyance@daase.net
gnome-extensions enable remove-alt-tab-delay@daase.net
gnome-extensions enable sound-output-device-chooser@kgshank.net
gnome-extensions enable trayIconsReloaded@selfmade.pl
gnome-extensions enable tweaks-system-menu@extensions.gnome-shell.fifi.org

notify-send "Garudas layout applied 🥰"

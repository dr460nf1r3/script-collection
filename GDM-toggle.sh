#!/bin/bash
echo "#!/bin/bash
export QT_QPA_PLATFORM=wayland
export _JAVA_AWT_WM_NONREPARENTING=1
export MOZ_ENABLE_WAYLAND=1 
export XDG_SESSION_TYPE=wayland
export EDITOR=/usr/bin/micro
export BROWSER=firefox
export TERM=gnome-terminal
export MAIL=geary
export QT_QPA_PLATFORMTHEME="qt5ct"" >> /etc/profile.d/gdm-wayland-toggled.sh

chmod +x /etc/profile.d/gdm-wayland-toggled.sh


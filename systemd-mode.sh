#!/bin/bash 
function goto
{
    label=$1
    cmd=$(sed -n "/^:[[:blank:]][[:blank:]]*${label}/{:a;n;p;ba};" $0 | 
          grep -v ':$')
    eval "$cmd"
    exit
}

function power
{
    sudo systemctl enable --now anbox-container-manager
    sudo systemctl enable --now libvirtd
    sudo systemctl enable --now docker
    sudo systemctl enable --now bluetooth-autoconnect
    sudo systemctl enable --now bluetooth
}

function battery
{
    sudo systemctl disable --now anbox-container-manager
    sudo systemctl disable --now libvirtd
    sudo systemctl disable --now docker
    sudo systemctl disable --now bluetooth-autoconnect
    sudo systemctl disable --now bluetooth
}

echo "Which systemd mode to enable?"
echo "1) Power"
echo "2) Battery"

: start
read mode

if [ $mode = "1" ]; then
    power
    echo ""
    echo "All systemd services enabled"
elif [ $mode = "2" ]; then
    battery
    echo ""
    echo "Stopped not needed systemd services for more battery"
else echo "There are only 2 options! :D"
    goto "start"
fi
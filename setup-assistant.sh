#!/bin/bash
function goto
{
    label=$1
    cmd=$(sed -n "/^:[[:blank:]][[:blank:]]*${label}/{:a;n;p;ba};" $0 | 
          grep -v ':$')
    eval "$cmd"
    exit
}

clear

echo "Welcome to the initial setup assistant!"
: start
echo "What do you want to do?"
echo ""
echo "Lite & Ultimate:"
echo "1) Get initial mirrorlist"
echo "2) Update the system"
echo "3) Debloat the system"
echo "4) Enable Adblocking / HiDPI mode / Guest user"
echo ""
echo "Lite only:"
echo "5) Fix missing characters by installing full noto-fonts packages"
echo "6) Enable Snap & Flatpak support"
echo "7) Enable Samba, printing & scanner support"
echo "8) Install gaming dependencies, Lutris & Steam"
echo ""
echo "9) Exit & delete this assistant"
echo ""

read selection

if [ $selection = "1" ]; then
    echo ""
    echo "Select options in Reflector Simple!"
    reflector-simple >/dev/null 2>&1
    echo ""
    goto "start"
elif [ $selection = "2" ]; then
    echo ""
    sudo pacman -Syu
    echo ""
    echo "System upgraded."
    echo ""
    goto "start"
elif [ $selection = "3" ]; then 
    echo ""
    echo "Follow the short tutorial!"
    echo "Make sure to inspect that removed packages first"
    firefox --new-window https://wiki.garudalinux.org/en/removing-bloat >/dev/null 2>&1 &
    pamac-manager >/dev/null 2>&1
    goto "start"
elif [ $selection = "4" ]; then
    echo ""
    echo "Find it in the settings tab."
    garuda-welcome >/dev/null 2>&1
    echo ""
    goto "start"
elif [ $selection = "5" ]; then
    echo ""
    sudo pacman -Syu --noconfirm noto-fonts-extra noto-fonts-cjk
    echo ""
    echo "Extra fonts should be installed now."
    goto "start"
elif [ $selection = "6" ]; then 
    echo ""
    sudo pacman -R --noconfirm pamac-aur pamac-tray-appindicator
    sudo pacman -Syu --noconfirm snapd snapd-glib apparmor flatpak pamac-all bauh pamac-tray-appindicator
    sudo systemctl enable --now snapd
    sudo systemctl enable --now apparmor 
    sudo systemctl enable --now snapd.apparmor
    echo ""
    echo "Snap & Flatpak support should now be enabled."
    goto "start"
elif [ $selection = "7" ]; then
    echo ""
    echo "Find it in the settings tab."
    garuda-welcome >/dev/null 2>&1
    echo ""
    goto "start"
elif [ $selection = "8" ]; then 
    echo ""
    sudo pacman -Syu --noconfirm gamemode lib32-gamemode lutris lutris-wine-meta steam steam-native-runtime linux-steam-integration steam-tweaks mangohud proton-tkg-git dxvk-mingw-git
    systemctl --user enable --now gamemoded
    echo ""
    echo "All gaming should now be installed."
    goto "start"
elif [ $selection = "9" ]; then 
    rm -r ~/.config/autostart-scripts/setup-assistant.sh
    echo "Script deleted."
    exit
else echo "Thats not a valid choice!"
    goto "start"
fi

cd /etc

echo "Applying Adguard DNS servers..."
mv resolv.conf resolv.conf~
sed -e '/nameserver/ c\nameserver 176.103.130.130' resolv.conf~ > resolv.conf

echo "Prevent resolv.conf from being overwritten"
chattr +i /etc/resolv.conf

echo "Adblocking enabled!"

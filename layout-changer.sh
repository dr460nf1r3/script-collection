#!/bin/bash

function goto
{
    label=$1
    cmd=$(sed -n "/^:[[:blank:]][[:blank:]]*${label}/{:a;n;p;ba};" $0 | 
          grep -v ':$')
    eval "$cmd"
    exit
}

echo "Choose a layout to apply:"
echo "1) Garuda"
echo "2) GNOME"
echo "3) Windows"
echo "4) PopOS-Tiling"

: start
read layout_num

if [ $layout_num = "1" ]; then 
    cd ~/Documents/Scripts
    sh ./garuda.sh
    echo "You chose Garuda as layout, applying.."
elif [ $layout_num = "2" ]; then
    cd ~/Documents/Scripts
    sh gnome.sh
    echo "You chose GNOME as layout, applying.."
elif [ $layout_num = "3" ]; then 
    cd ~/Documents/Scripts
    sh windows.sh
    echo "You chose Windows as layout, applying.."
elif [ $layout_num = "4" ]; then 
    cd ~/Documents/Scripts
    sh pop-tiling.sh
    echo "You chose PopOS-Tiling as layout, applying.."
else echo "Thats not a valid choice!"
    goto "start"
fi
#!/bin/bash

# This script updates the wallpaper in GNOME based DE.
# Add the line: sh <file-path-of-this-file> &
# in the ~/.profile to start the script after login.
# Placing this in crontab or init.d doesn't work as they doesn't
# have necessary environment variables.

TIME_INTERVAL=30 # Seconds

gsettings set org.gnome.desktop.background picture-uri file:///home/nico/Bilder/Wallpaper/pscircle.png

. "${HOME}/.cache/wal/colors.sh"

reload_pscircle() {
    pscircle \
        --background-color=${background:1} \
        --tree-font-color=${color8:1} \
        --dot-color-min=${color4:1} \
        --dot-color-max=${color4:1} \
        --dot-border-color-min=${color15:1} \
        --dot-border-color-max=${color15:1} \
        --link-color-min=${color0:1} \
        --link-color-max=${color0:1} \
        --toplists-font-color=${color2:1} \
        --toplists-pid-font-color=${color10:1} \
        --toplists-bar-background=${color0:1} \
        --toplists-bar-color=${color2:1} \
        --output=${HOME}/Bilder/Wallpaper/pscircle.png &
}

while [ 1 ]; do
    # Replace the next line with any parameters given in the examples.
    reload_pscircle
    sleep $TIME_INTERVAL
done
